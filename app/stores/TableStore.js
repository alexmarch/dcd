import Reflux from 'reflux';
import {TableActions} from '../actions/Table/TableActions.js';

const TableStore = Reflux.createStore({
    listenables: [TableActions],
    collapseRow(rowIdx){
        this.trigger({ collapseRow: rowIdx }); 
    },
    fillGridData(data){
        this.trigger({ gridData: data });
    },
    collapseAll(){
       this.trigger({ collapseAll: true });  
    }
});

export default TableStore;