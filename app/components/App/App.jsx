import React, {Component} from 'react';
import THead from '../Table/THead.jsx';
import TBody from '../Table/TBody.jsx';
import Grid from '../Table/Grid.jsx';
import TableStore from '../../stores/TableStore.js';
import Button from 'react-bootstrap/lib/Button.js';
import GridToolbox from '../GridToolbox/GridToolbox.jsx';
import Preloader from '../Preloader/Preloader.jsx';
import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import AppBar from 'material-ui/AppBar';
import Reflux from 'reflux';
import './app.sass';

/**
 * Application component
 */

class App extends Component {

    state = { gridData: {} }

    componentDidMount(){
        TableStore.listen(this.fillGrdData.bind(this));
    }
    fillGrdData(data){
        if(data.gridData){
            this.setState({gridData: data.gridData});
        }
    }
    render(){
        const topHeaderLabels = ['','EUR/USD','EUR/GBP','EUR/JPY','EUR/AUD','EUR/SGD'];
        const hederLabel = ['EUR','USD','GBP','JPY','AUD','SGD'];
        const styles = {
            background: '#f9f9f9',
            padding: 0
        };
        return (
            <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
                <div className="container-fluid" style={styles}>
                     <AppBar
                        title="Dual Currency Deposit"
                        iconClassNameRight="muidocs-icon-navigation-expand-more"
                    />
                    <div className="grid-container">
                        <GridToolbox />
                        <Preloader isData={this.state.gridData.rows}>
                            <Grid 
                                striped bordered condensed hover topCtrls={this.state.gridData.rows ? true : false}
                                head={this.state.gridData.rows &&
                                    [
                                        <THead gridRowEmpty key="head_1" items={topHeaderLabels}></THead>,
                                        <THead key="head_2" items={hederLabel}></THead>
                                    ]
                                }
                                body={this.state.gridData}
                                />
                        </Preloader>
                    </div>
                </div>
            </MuiThemeProvider>
        );
    }
}
export default App;