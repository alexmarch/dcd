import React, {Component} from 'react';
import PreloaderActions from '../../actions/Preloader/PreloaderActions.js';
import CircularProgress from 'material-ui/CircularProgress';
import './preloader.sass';

class Preloader extends Component{
    state = { isLoading: false }
    componentDidMount(){
        PreloaderActions.startPreloader.listen(()=>{
            this.setState({isLoading: true});
        });
        PreloaderActions.stopPreloader.listen(()=>{
            this.setState({isLoading: false});
        });
    }
    render(){
        return(
            <div className="preloader">
                { !this.props.isData && this.state.isLoading ? <CircularProgress size={1.5} /> : null }
                {  this.props.isData && !this.state.isLoading && this.props.children }
            </div>
        );
    }
};

export default Preloader;