import React, {Component} from 'react';
import TRow from './TRow.jsx';
import TableStore from '../../stores/TableStore.js';
import {Table, TableBody} from 'material-ui/Table';
import Reflux from 'reflux';

class TBody extends Component {
    componentDidMount(){
        TableStore.listen(this.expandTableRow);
    }
    expandTableRow(params){
        if(params.hasOwnProperty('collapseRow')){
            if(this.props.rows[params.collapseRow]){
                this.props.rows[params.collapseRow].isCollapsed = !this.props.rows[params.collapseRow].isCollapsed;
                this.forceUpdate();
            }
        }
        if(params.collapseAll){
            this.props.rows.forEach((row)=>{
                row.isCollapsed = true;
            });
            this.forceUpdate();
        }
    }
    render(){
        return(
            <TableBody className="grid__body">
                {this.props.children}
                {this.props.rows && this.props.rows.map((row, idx)=>{
                    if(row.collapsed){
                        return ([
                            <TRow key={'#row' + idx} idx={idx} {...row} />,
                            <TRow hidden key={'#row-collapsed' + idx} idx={idx} {...row} />
                        ]);
                    } else {
                        return ([
                            <TRow key={'#row' + idx} idx={idx} {...row} />
                        ]);
                    }
                })}
            </TableBody>
        )
    }
};

export default TBody;