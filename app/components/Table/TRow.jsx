import React, {Component} from 'react';
import Button from 'react-bootstrap/lib/Button';
import ReactDOM from 'react-dom';
import classNames from 'classnames';
import {TableActions} from '../../actions/Table/TableActions.js';
import {TableRow, TableRowColumn} from 'material-ui/Table';
import IconButton from 'material-ui/IconButton';
import FlatButton from 'material-ui/FlatButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import NavigationArrowDropDown from 'material-ui/svg-icons/navigation/arrow-drop-down';
import TBody from './TBody.jsx';
import Grid from './Grid.jsx';
import _ from 'underscore';

class TRow extends Component{

    state = { isExtended: false }

    expandRow(){
       TableActions.collapseRow(this.props.idx);
    }
    isHeaderRow(){
        return this.props.legend && !this.props.hidden;
    }
    isCollapsed(){
        return this.props.hidden && this.props.isCollapsed
    }
    render(){
        let rowStyles = {
            height: 'auto'
        };
        let body = { rows: {} };
        return(
            <TableRow style={rowStyles}>
                { this.isHeaderRow() &&
                    <TableRowColumn className="grid__col grid__col--legend">
                        {(this.props.collapsed && !this.props.hidden && !this.isCollapsed()) ? 
                            <FlatButton 
                                icon={<NavigationArrowDropDown />} 
                                label={this.props.legend}
                                labelPosition="after"
                                onClick={this.expandRow.bind(this)} /> : <span>{this.props.legend}</span>
                        }
                    </TableRowColumn> 
                }
                {(this.props.data && !this.props.hidden) && this.props.data.map((value, idx)=>{
                    return <TableRowColumn  key={'#rtd-' + idx} className="grid__col table-data">{value}</TableRowColumn>
                })}
                {(this.isCollapsed() && this.props.rowData) &&
                     <TableRowColumn colSpan={this.props.data.length + 1} className="grid__col grid__col--collapsed table-data ">
                        <Grid body={ _.extend(body, {rows: this.props.rowData}) } />
                     </TableRowColumn>
                }
            </TableRow>
        )
    }
};

export default TRow