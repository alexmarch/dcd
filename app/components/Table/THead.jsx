import React, {Component} from 'react';
import classNames from 'classnames';
import {TableRow, TableHeaderColumn} from 'material-ui/Table';

import './thead.sass';

/**
 * Inline editr for grid head th 
 */
const InlineEdit = ({isActive, value, activeClass, done})=>{
    return(
        <div className="inline-edit">
            <span className={classNames({ 'hide': isActive })}>
                {value}
            </span>
            <input type="text"
                className={classNames({ 'hide': !isActive, 'form-control': true })} 
                defaultValue={value} onBlur={(event)=>{
                    done(event.currentTarget.value);
            }} />
        </div>
    );
}

class THead extends Component {

    state = { isEdit: false }

    editClick(index, event){
        this.setState({
            isEdit: true,
            editIndex: index
        });
    }
    editDone(value){
        this.props.items[this.state.editIndex] = value;
        this.setState({ isEdit: false});
    }
    isEditable(idx){
        return this.state.isEdit && this.state.editIndex === idx;
    }
    render(){
        return(
                <TableRow className={classNames('grid__head grid__row',{'grid__row--empty-bg': this.props.gridRowEmpty})}>
                    {this.props.items && this.props.items.map((item, idx)=>{
                        return (<TableHeaderColumn className="grid__col" key={'#' + idx} onClick={this.editClick.bind(this, idx)}>
                                 <InlineEdit
                                    isActive={this.isEditable(idx)}
                                    activeClass='th-edit'
                                    value={item}
                                    done={this.editDone}
                                 ></InlineEdit>
                               </TableHeaderColumn>)
                    })}
                </TableRow>
        );
    }
};

export default THead;