import React, {Component} from 'react';
import {TableActions} from '../../actions/Table/TableActions.js';
import TRow from './TRow.jsx';
import {Table, TableBody, TableHeader} from 'material-ui/Table';
import RaisedButton from 'material-ui/RaisedButton';
import NavigationArrowDropDown from 'material-ui/svg-icons/navigation/arrow-drop-down';
import TableStore from '../../stores/TableStore.js';
import Reflux from 'reflux';

import './grid.sass';

class Grid extends Component {
    componentDidMount(){
        TableStore.listen(this.expandTableRow.bind(this));
        this.allCollapsed = false;
    }
    expandTableRow(params){
        if(params.hasOwnProperty('collapseRow')){
            if(this.props.body.rows[params.collapseRow]){
                this.props.body.rows[params.collapseRow].isCollapsed = !this.props.body.rows[params.collapseRow].isCollapsed;
                this.forceUpdate();
            }
        }
        if(params.collapseAll){
            this.allCollapsed = !this.allCollapsed;
            this.props.body.rows.forEach((row)=>{
                row.isCollapsed = this.allCollapsed;
            });
            this.forceUpdate();
        }
    }
    render(){
       return (
           <div>
            {(!this.props.noexpend && this.props.topCtrls) && <div className="row"><div className="col-md-12"><RaisedButton label="Expand all" onClick={TableActions.collapseAll} labelPosition="after" icon={<NavigationArrowDropDown />} primary={true} className="pull-right"></RaisedButton></div></div>}
            <div className="row">  
                <div className="col-md-12">  
                    <Table children={this.props.body}>
                            {this.props.head && <TableHeader>{this.props.head}</TableHeader> }
                            <TableBody className="grid__body">
                            {this.props.body.rows && this.props.body.rows.map((row, idx)=>{
                                if(row.collapsed){
                                    return ([
                                        <TRow key={'#row' + idx} idx={idx} {...row} />,
                                        <TRow hidden key={'#row-collapsed' + idx} idx={idx} {...row} />
                                    ]);
                                } else {
                                    return ([
                                        <TRow key={'#row' + idx} idx={idx} {...row} />
                                    ]);
                                }
                            })}
                            </TableBody>
                    </Table>
                </div>
            </div>
           </div>
       );
    }
};

export default Grid;