import React from 'react';
import GridLeftToolbox from './GridLeftToolbox.jsx';
import GridRightToolbox from './GridRightToolbox.jsx';
import 'whatwg-fetch';

import './grid-toolbox.sass';

const GridToolbox = React.createClass({
    componentDidMount(){
        fetch('data/counterpart.json').then(res=>{
            return res.json();
        }).then(json=>{
            this.counterpartData = json;
            if(json.data && json.data.length > 0){
                this.setState({ counterpartValue : json.data[0] });
            }
            this.forceUpdate();
        });
        fetch('data/ccy.json').then(res=>{
            return res.json();
        }).then(json=>{
            this.currency = json.currency; 
            this.setState({ currencyValue : json.currency[0] });   
        });
    },
    getInitialState(){
        return { counterpartValue: null, currencyValue: null };
    },
    onChangeCounterpart(value){
        this.setState({ counterpartValue : value });
    },
    updateCurrency(value){
        this.setState({ currencyValue : value });
    },
    render(){
        return(
            <div className="grid-toolbox">
                <div className="row">
                    <GridLeftToolbox 
                        updateCurrency={this.updateCurrency}
                        currencyOptions={this.currency}
                        currencyValue={this.state.currencyValue}
                        counterpartValue={this.state.counterpartValue}
                        counterpartData={this.counterpartData}  
                        onChangeCounterpart={this.onChangeCounterpart}/>
                    <GridRightToolbox 
                        currencyValue={this.state.currencyValue}
                        counterpartValue={this.state.counterpartValue} />
                </div>
            </div>
        )
    }
});

export default GridToolbox;