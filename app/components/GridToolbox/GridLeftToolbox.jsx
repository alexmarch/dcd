import React, {Component} from 'react';
import Select from 'react-select';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import TextField from 'material-ui/TextField';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';

import 'react-select/dist/react-select.css';

class GridLeftToolbox extends Component{
    state = {
            counterpartValue: 0,
            depositCurrency: 'EUR',
            maturity: '1_week',
            nominal: '',
            ounterpartOpen: false,
            counterpartCurValue: null,
            depositCurCurrency: null
    };
    changeCounterpartHandler(value) {
        this.setState({ counterpartValue: value });
    }
    handleClose(){
        this.setState({ounterpartOpen: false});
    }
    applyValue(){
        let value = this.state.counterpartCurValue;
        this.props.onChangeCounterpart(this.props.counterpartData.data[value]);
        this.changeCounterpartHandler(value);
        this.setState({ounterpartOpen: false});
    }
    render() {
        const ounterpartActions = [
            <FlatButton
                label="Apply"
                primary={true}
                onTouchTap={this.applyValue.bind(this)}
            />,
            <FlatButton
                label="Cancel"
                primary={true}
                onTouchTap={this.handleClose.bind(this)}
            />
        ];
        return (
            <div className="col-md-5">
                <div className="row">
                    <div className="col-md-6">
                        <SelectField floatingLabelText="Counterpart" value={this.state.counterpartValue} onChange={(event, index, value) => {
                            if(this.state.counterpartValue !== value){
                                this.setState({ounterpartOpen: true, counterpartCurValue: value});
                            }
                        } }>
                            {this.props.counterpartData && this.props.counterpartData.data.map((item, idx) => {
                                return <MenuItem key={item.name} value={idx} primaryText={item.name}></MenuItem>
                            }) }
                        </SelectField>
                        <Dialog actions={ounterpartActions} open={this.state.ounterpartOpen} modal={true} onRequestClose={this.handleClose}>
                            Input parameters changed. Do you want to refresh pricing?
                        </Dialog>
                    </div>
                    <div className="col-md-2">Tier { this.props.counterpartValue && this.props.counterpartValue.tier }</div>
                </div>
                <div className="row">
                    <div className="col-md-5">
                        <SelectField floatingLabelText="Deposit Currency" value={this.state.depositCurrency} onChange={(event, index, value) => {
                            this.props.updateCurrency(value);
                            this.setState({depositCurrency: value});
                        }}>
                            {this.props.currencyOptions && this.props.currencyOptions.map(item => {
                                return <MenuItem key={item} value={item} primaryText={item}></MenuItem>
                            }) }
                        </SelectField>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-5">
                        <SelectField floatingLabelText="Maturity" value={this.state.maturity} onChange={(event, index, value) =>{
                            this.setState({maturity: value});
                        }} >
                            <MenuItem value="1_week"    primaryText="1 week"></MenuItem>
                            <MenuItem value="2_weeks"   primaryText="2 weeks"></MenuItem>
                            <MenuItem value="3_weeks"   primaryText="3 weeks"></MenuItem>
                            <MenuItem value="1_month"   primaryText="1 month"></MenuItem>
                            <MenuItem value="2_months"  primaryText="2 months"></MenuItem>
                            <MenuItem value="3_months"  primaryText="3 months"></MenuItem>
                        </SelectField>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-5">
                        <TextField hintText="Enter nominal count" floatingLabelText={'$ Nominal ' + this.props.currencyValue} value={this.state.nominal} onChange={event=>{
                            this.setState({nominal: event.target.value});
                        }} />
                    </div>
                </div>
            </div>
        );
    }
};

export default GridLeftToolbox;