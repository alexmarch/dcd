import React from 'react';
import { generateGridData } from '../../actions/Table/TableActions.js';
import RaisedButton from 'material-ui/RaisedButton';
import ImageGridOn from 'material-ui/svg-icons/image/grid-on.js';

const GridRightToolbox = (props)=>{
    return(
        <div className="col-md-4">
            <div className="row">
                <div className="col-md-5">
                    Margin
                </div>
                <div className="col-md-3">
                    {props.counterpartValue ? props.counterpartValue.margin : 0}%
                </div>
            </div>
            <div className="row">
                <div className="col-md-5">
                    VA Amount
                </div>
                <div className="col-md-3">
                    $0.00
                </div>
                <div className="col-md-3">
                    {props.currencyValue}
                </div>
            </div>
            <div className="row">
                <div className="col-md-9">
                    <RaisedButton icon={<ImageGridOn/>}  label="Price"  secondary={true} onClick={generateGridData} />
                </div>
            </div>
        </div>
    )
};

export default GridRightToolbox;