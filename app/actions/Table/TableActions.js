import Reflux from 'reflux';
import 'whatwg-fetch';
import PreloaderActions from '../Preloader/PreloaderActions.js';

const TableActions = Reflux.createActions(
    [
        'collapseAll',
        'collapseRow', 
        'fillGridData'
    ]
);

console.log('PreloaderActions',PreloaderActions);

function getJSONData(file){
    return fetch(file).then(res=>{
        return res.json(); 
    })
};

function getATM(cb) {
    getJSONData('data/eurusd_dcd_mATM.json')
        .then(data=>{
            typeof cb === 'function' && cb(data);
        });
}

function get15D(cb) {
    getJSONData('data/eurusd_dcd_m15D.json')
        .then(data=>{
            typeof cb === 'function' && cb(data);
        });
}

function get25D(cb) {
    getJSONData('data/eurusd_dcd_m25D.json')
        .then(data=>{
            typeof cb === 'function' && cb(data);
        });
}

function formatNumber(num) {
   return Number(Number(num).toFixed(4)).toLocaleString()
}

function initData(data) {
    let {allData, dataATM, data15D,  data25D } = data;
    
    //Fill ATM data 
    let outputs = dataATM.xmlProduct.outputs;
    allData.rows[0].data[0] = Number(outputs['Option.Ref_spot']).toFixed(4);
    allData.rows[1].data[0] = Number(outputs['Loan.Rate_Margin']).toFixed(4);
    allData.rows[1].rowData[0].data[0] =  Number(outputs['Option.Strike']).toFixed(4);
    allData.rows[1].rowData[1].data[0] =  formatNumber(outputs['Loan.Repayment1']);
    allData.rows[1].rowData[2].data[0] =  formatNumber(outputs['Loan.Repayment2']);

    //Fill 15D data
    outputs = data15D.xmlProduct.outputs;
    allData.rows[2].data[0] = Number(outputs['Loan.Rate_Margin']).toFixed(4);
    allData.rows[2].rowData[0].data[0] =  Number(outputs['Option.Strike']).toFixed(4);
    allData.rows[2].rowData[1].data[0] =  formatNumber(outputs['Loan.Repayment1']);
    allData.rows[2].rowData[2].data[0] =  formatNumber(outputs['Loan.Repayment2']);

    //Fill 25D data
    outputs = data25D.xmlProduct.outputs;
    allData.rows[3].data[0] = Number(outputs['Loan.Rate_Margin']).toFixed(4);
    allData.rows[3].rowData[0].data[0] =  Number(outputs['Option.Strike']).toFixed(4);
    allData.rows[3].rowData[1].data[0] =  formatNumber(outputs['Loan.Repayment1']);
    allData.rows[3].rowData[2].data[0] =  formatNumber(outputs['Loan.Repayment2']);

    PreloaderActions.stopPreloader();
    TableActions.fillGridData(allData);
}

export function generateGridData(){
    PreloaderActions.startPreloader();
    getJSONData('data/data.json')
        .then(allData=>{
            if(allData){
                getATM(dataATM=>{
                    PreloaderActions.stopPreloader();
                    TableActions.fillGridData(dataATM);
                    get15D(data15D=>{
                        get25D(data25D=>{
                            initData({ allData, dataATM, data15D, data25D });
                        });
                    });
                });
            }
        });
}


export { TableActions, generateGridData};