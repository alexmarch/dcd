import Reflux from 'reflux';
const PreloaderActions = Reflux.createActions([
    'startPreloader',
    'stopPreloader'
]);
export default  PreloaderActions;