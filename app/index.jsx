import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App/App.jsx';
import 'bootstrap/dist/css/bootstrap.css';
import injectTapEventPlugin from 'react-tap-event-plugin';

injectTapEventPlugin();

var appNode = document.getElementById('app');
ReactDOM.render(<App/>, appNode);
