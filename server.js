const WebpackDevServer = require("webpack-dev-server");
const webpack = require("webpack");
const open = require('open');

const config = require(`./configs/webpack.${process.env.NODE_ENV ? process.env.NODE_ENV : 'dev' }.js`);

config.entry.app.unshift(
  "webpack-dev-server/client?http://0.0.0.0:8080", 
  'webpack/hot/only-dev-server'
  );

const server = new WebpackDevServer(webpack(config), {
  contentBase: './app',
  hot: true,
  inline: false,
  noInfo: false,
  publicPath: '/',
  historyApiFallback: true,
  stats: { colors: true },
  open: true
});
const port = process.env.PORT || 8080;

server.listen(port, "localhost", ()=>{
    console.log(`Server started at port ${port} !`);
    open('http://localhost:' + port);
});

